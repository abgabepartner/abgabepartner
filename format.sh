#!/bin/sh
FORMAT="hfmt -w"

${FORMAT} app/**.hs
${FORMAT} src/**.hs
${FORMAT} test/**.hs
