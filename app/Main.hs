{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Web.Spock
import           Web.Spock.Config

data AbgabepartnerSession =
  EmptySession

data AbgabepartnerState =
  EmptyState ()

main :: IO ()
main = do
  spockCfg <- defaultSpockCfg EmptySession PCNoDatabase (EmptyState ())
  runSpock 8080 (spock spockCfg app)

app :: SpockM () AbgabepartnerSession AbgabepartnerState ()
app = do
  get root $ text "Hello, World!"
  get ("hello" <//> var) $ \name -> text ("Hello " <> name <> "!")
