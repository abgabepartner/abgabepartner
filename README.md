
Hier entsteht das neue Backend für abgabepartner.de.

- [Installation](#installation)
	- [Paketverwaltung / `stack`](#paketverwaltung--stack)
	- [Entwicklung](#entwicklung)
	- [Bibliotheken](#bibliotheken)
- [Lizenz](#lizenz)

# Installation
Es handelt sich um ein Haskell-Projekt, es kann via

	stack build --fast --pedantic
	stack exec apb-exe

installiert und ausgeführt werden.

## Paketverwaltung / `stack`
Der Build-Prozess wird mittels `stack` (`stack` ist eine kompatible Alternative zu `cabal`) durchgeführt.
Der bevorzugte Compiler ist `ghc`, beispielsweise in Version `8.8`, unabhängig installiert lässt sich dies via `ghc --version` prüfen.
Wie auch jede Abhängigkeit wird der Compiler automatisch durch `stack` installiert.
[stack](https://docs.haskellstack.org/en/stable/README/) wiederum kann durch einen der beiden Befehle 

	curl -sSL https://get.haskellstack.org/ | sh
	wget -qO- https://get.haskellstack.org/ | sh

installiert werden. Bereits installiert lässt es sich leicht aktualisieren:

	stack upgrade

Eventuell muss die Umgebungsvariable `PATH` erweitert werden, `stack` liefert in dem Fall eine entsprechende Warnung.

> Bei der Verwendung von `stack` kann es vorkommen, dass zuvor zusätzliche Abhängigkeiten in der globalen Konfiguration (`~/.stack/global-project/stack.yaml` falls nicht anders angegeben) hinzugefügt werden müssen ([siehe](https://stackoverflow.com/a/57247211/2909111)):
>
>		extra-deps:
>		  - name-version
>		  - name-version
>		  …
>
> Die entsprechenden Details werden in der auftretenden Fehlermeldung genannt.
> Beispielsweise, kann das so aussehen:
> 
> 		extra-deps:
>		  - hindent-5.3.1
>		  - stylish-haskell-0.9.3.0
>
> Weiterhin wird `lts-14.9` als `resolver` genutzt.

## Entwicklung

Für die automatische Formatierung wird [hfmt](http://hackage.haskell.org/package/hfmt) genutzt, welches via

	stack install hfmt

installiert werden kann. Mittels `format.sh` wird dieses auf den entsprechenden Dateien mit einheitlicher Konfiguration ausgeführt.

> Alternativ kann `hftm` auch via `cabal` installiert werden.
> Im allgemeinen hat `stack` den Vorteil, dass Konflikte zwischen Abhängigkeiten durch Umgebungen verhindert werden sollen.
> Insgesamt soll dadurch die Installation unabhängig vom System stets gleich aussehen ([siehe](http://docs.haskellstack.org/en/stable/lock_files/)).
> Die Menge an benötigtem Speicher kann dadurch leider größer ausfallen, bei Problemen kann `cabal` versucht werden.
>
>		cabal new-install hfmt
>
> Hierbei sollte beachtet werden, dass `cabal` ggf. zunächst [konfiguriert](https://www.haskell.org/cabal/users-guide/installing-packages.html) werden muss.
> Gewöhnlicherweise ist unter `~/.cabal/config` die Konfiguration hinterlegt.

## Bibliotheken
Wir verwenden das Framework [Spock](https://www.spock.li/), ein [Tutorial](https://www.spock.li/tutorials/getting-started) ist auf der Webseite vorhanden.

	stack install Spock

Beachte, dass `package.yaml` (statt `apb.cabal`) bei Abhängigkeits-Änderungen editiert werden muss.

# Lizenz
Bitte beachte die Textdatei `LICENSE`, welche diesem Projekt beigelegt ist.
